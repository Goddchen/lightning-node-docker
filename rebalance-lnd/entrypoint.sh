#!/bin/bash
[ -z "$LND_HOST" ] && echo "Need to set LND_HOST environment variable" && exit 1;
[ ! -r ~/.lnd/tls.cert ] && echo "Need to mount tls.cert into ~/.lnd/tls.cert" && exit 1;
[ ! -r ~/.lnd/data/chain/bitcoin/mainnet/admin.macaroon ] && echo "Need to mount admin.macaroon into ~/.lnd/data/chain/bitcoin/mainnet/admin.macaroon" && exit 1;

cd /rebalance-lnd/

sed -i "s/localhost:10009/$LND_HOST/g" lnd.py

exec ./rebalance.py $@