if(!process.env.LND_HOST) {
    console.error("LND_HOST not set!");
    process.exit(1);
}

const grpcProtoLoader = require('@grpc/proto-loader');
const grpc = require('grpc');
const fs = require('fs');
const express = require('express');
const app = express();
const grpcPromisify = require('grpc-promisify');

const lndCert = fs.readFileSync('tls.cert');
const readOnlyMacaroon = fs.readFileSync('readonly.macaroon');
const readOnlyMacaroonHex = readOnlyMacaroon.toString('hex');
const meta = new grpc.Metadata();
meta.add('macaroon', readOnlyMacaroonHex);
const macaroonCredentials = grpc.credentials.createFromMetadataGenerator((_args, callback) => {
    callback(null, meta);
});
const sslCredentials = grpc.credentials.createSsl(lndCert);
const credentials = grpc.credentials.combineChannelCredentials(sslCredentials, macaroonCredentials);

const packageDefinition = grpcProtoLoader.loadSync('rpc.proto');
const lnrpc = grpc.loadPackageDefinition(packageDefinition);

console.log("RPC host: " + process.env.LND_HOST);
var lightning = new lnrpc.lnrpc.Lightning(process.env.LND_HOST, credentials);
grpcPromisify(lightning);

app.get('/metrics', async (req, res) => {
    (async () => {
        try {
            const info = await lightning.GetInfo({});
            const walletBalance = await lightning.WalletBalance({});
            const channelBalance = await lightning.ChannelBalance({});
            const networkInfo = await lightning.GetNetworkInfo({});
            res.contentType('text/plain');
            res.send(
                `lnd_info_num_active_channels ${info.numActiveChannels || 0}\n` +
                `lnd_info_num_pending_channels ${info.numPendingChannels || 0}\n` +
                `lnd_info_num_peers ${info.numPeers || 0}\n` +
                `lnd_info_block_height ${info.blockHeight || 0}\n` +
                `lnd_info_synced_to_chain ${info.syncedToChain || 0 ? 1 : 0}\n` +
                `lnd_info_num_inactive_channels ${info.numInactiveChannels || 0}\n` +
                `lnd_wallet_balance_total ${walletBalance.totalBalance || 0}\n` +
                `lnd_wallet_balance_confirmed ${walletBalance.confirmedBalance || 0}\n` +
                `lnd_wallet_balance_unconfirmed ${walletBalance.unconfirmedBalance || 0}\n` +
                `lnd_channel_balance ${channelBalance.balance || 0}\n` +
                `lnd_channel_balance_pending_open ${channelBalance.pendingOpenBalance || 0}\n` +
                `ln_network_info_graph_diameter ${networkInfo.graphDiameter || 0}\n` +
                `ln_network_info_avg_out_degree ${networkInfo.avgOutDegree || 0}\n` +
                `ln_network_info_max_out_degree ${networkInfo.maxOutDegree || 0}\n` +
                `ln_network_info_num_nodes ${networkInfo.numNodes || 0}\n` +
                `ln_network_info_num_channels ${networkInfo.numChannels || 0}\n` +
                `ln_network_info_total_network_capacity ${networkInfo.totalNetworkCapacity || 0}\n` +
                `ln_network_info_avg_channel_size ${networkInfo.avgChannelSize || 0}\n` +
                `ln_network_info_min_channel_size ${networkInfo.minChannelSize || 0}\n` +
                `ln_network_info_max_channel_size ${networkInfo.maxChannelSize || 0}`);
        } catch (err) {
            console.error(err);
            res.status(500).send(err);
        }
    })();
});

app.listen(9550, () => console.log('Started on port 9550'));